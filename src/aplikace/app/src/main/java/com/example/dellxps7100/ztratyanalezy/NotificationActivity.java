package com.example.dellxps7100.ztratyanalezy;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;

import java.util.ArrayList;

import in.myinnos.awesomeimagepicker.helpers.ConstantsCustomGallery;
import in.myinnos.awesomeimagepicker.models.Image;

/**
 * Created by Patrik on 02.05.2018.
 */

public class NotificationActivity extends AppCompatActivity {
    private static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        MenuListener menu = new MenuListener();
        menu.listen(this.findViewById(android.R.id.content), getApplicationContext());
        callCategoryPicker();
        googleMapPicker();
        Button add = (Button) findViewById(R.id.addNotification);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addingButtonController();
            }
        });
        Button delete = (Button) findViewById(R.id.deleteNotifications);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteButtonController();
            }
        });

    }

    public void callCategoryPicker() {
        Spinner spin = (Spinner) findViewById(R.id.categoriesSpinner);
        ArrayAdapter<CharSequence> types = ArrayAdapter.createFromResource(this,
                R.array.categoriesArray, android.R.layout.simple_spinner_item);
        types.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(types);
    }

    public void googleMapPicker(){
        final TextView place = (TextView) findViewById(R.id.placeText);
        place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleAutoCompleteStart();
            }
        });
    }

    private void googleAutoCompleteStart(){
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(NotificationActivity.this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Toast.makeText(getApplicationContext(), getResources()
                    .getString(R.string.googleMapsError), Toast.LENGTH_LONG).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getApplicationContext(), getResources()
                    .getString(R.string.googleMapsError), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resCode == RESULT_OK) {
                googlePlaceWasPick(data);
            } else if (resCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resCode == RESULT_CANCELED) {}
        }
    }

    private void googlePlaceWasPick(Intent data){
        Place place = PlaceAutocomplete.getPlace(this, data);
        final TextView placeText = (TextView) findViewById(R.id.placeText);
        placeText.setText(place.getAddress().toString());
        Log.i("MAPA", "Place: " + place.getName());
    }

    private void addingButtonController(){
        Spinner category = (Spinner) findViewById(R.id.categoriesSpinner);
        String categoryString = category.getSelectedItem().toString();
        TextView place = (TextView) findViewById(R.id.placeText);
        String placeText = place.getText().toString();
        if (placeText == ""){
            place.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_error_24dp, 0);
        }
        else {
            addNotification(categoryString, placeText);
        }

    }

    private void deleteButtonController(){
        SharedPreferences prefs = this.getSharedPreferences("notif", MODE_PRIVATE);
        SharedPreferences.Editor editorNotif = prefs.edit();
        editorNotif.clear();
        editorNotif.commit();
        Toast.makeText(this, getResources().getString(R.string.notifDeleted), Toast.LENGTH_SHORT).show();
    }


    private void addNotification(String place, String category){
        NotifObject nO = new NotifObject(category, place);
        SharedPreferences prefs = this.getSharedPreferences("notif", MODE_PRIVATE);
        SharedPreferences indexes = this.getSharedPreferences("indexes", MODE_PRIVATE);
        int index;
        //potrebuju unikatni klic v sharedpreferences
        if (indexes.getString("index", null) == null){
            SharedPreferences.Editor editorIndex = indexes.edit();
            editorIndex.putString("index", "1");
            editorIndex.commit();
            index = Integer.parseInt(indexes.getString("index", null));
        }
        else {
            index = Integer.parseInt(indexes.getString("index", null));
            SharedPreferences.Editor editorIndex = indexes.edit();
            int actual = Integer.parseInt(indexes.getString("index", null));
            actual++;
            editorIndex.putString("index", Integer.toString(actual));
            editorIndex.commit();
        }
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(nO);
        System.out.println(json);
        editor.putString("notifObject" + index, json);
        editor.commit();
        Toast.makeText(this, getResources().getString(R.string.notifAdded), Toast.LENGTH_SHORT).show();
    }













}
