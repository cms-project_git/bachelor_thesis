package com.example.dellxps7100.ztratyanalezy;

import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 22.03.2018.
 */

public class UploadImageThread implements Runnable{

    private String fileName;
    private String imgPath;
    private NewThingActivity activity;

    UploadImageThread(String thingId, String imgPath, NewThingActivity activity){
        this.fileName = thingId;
        this.imgPath = imgPath;
        this.activity = activity;
    }

    @Override
    public void run() {
        uploadImageToServer(this.fileName, this.imgPath);
    }

    private void uploadImageToServer(String thingId, String imgPath){
        String fileType = ".jpg";
        File imageFile = new File(imgPath);
        try {
            AuthInterceptor auth = new AuthInterceptor(activity);
            File compressedFile = new Compressor(activity.getApplicationContext()).compressToFile(imageFile);
            RequestBody image = RequestBody.create(MediaType.parse("image/*"), compressedFile);
            MultipartBody.Part multi = MultipartBody.Part.createFormData("image", thingId + fileType, image);
            RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), "upload");
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(auth).build();

            ServerInfo server = new ServerInfo();
            UploadImageInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                    .client(client).build().create(UploadImageInterface.class);

            retrofit2.Call<okhttp3.ResponseBody> request = service.postImage(multi, textBody);
            request.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    System.out.println("DONE");
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    System.out.println(t.getMessage().toString());
                    activity.showImageErrorToast();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
