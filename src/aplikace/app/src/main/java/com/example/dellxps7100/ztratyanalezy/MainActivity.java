package com.example.dellxps7100.ztratyanalezy;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.PreferenceChangeEvent;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity implements SetToken{

    SharedPreferences notifPref;
    SharedPreferences alreadyNotified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MenuListener menuListener = new MenuListener();
        menuListener.listen(this.findViewById(android.R.id.content), getApplicationContext());
        final SharedPreferences pref = this.getSharedPreferences("auth", MODE_PRIVATE);
        notifPref = this.getSharedPreferences("notif", MODE_PRIVATE);
        alreadyNotified = this.getSharedPreferences("alreadyNotified", MODE_PRIVATE);
        final SharedPreferences indexes = this.getSharedPreferences("indexes", MODE_PRIVATE);
        Login getToken = new Login("admin", "admin", this);
        getToken.makeLogin();
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
               checkNotifications();
            }

        }, 0, 5000); //5 vterin
    }


    public void checkNotifications(){
        Map<String,?> keys = notifPref.getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            String oneItem = entry.getValue().toString();
            String key = entry.getKey();
            try {
                JSONObject json = new JSONObject(oneItem);
                String category = json.get("category").toString();
                String place = json.get("address").toString();
                String date = json.get("date").toString();
                Notification request = new Notification(this, place, category, date, key);
                request.makeNotification();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void checkIfNotify(String response, String place, String category, String key){
        try {
            System.out.println("check-if-notify");
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                MObject thing = new MObject();
                thing.obJsonToMObj(obj);
                if ((thing.getPlace().equals(place)) && (!(checkIfAlreadyNotified(thing.getId())))) {
                    notification(thing, category, place);
                }
                else if (((extractCity(thing.getPlace())).equals(extractCity(place))) && (!(checkIfAlreadyNotified(thing.getId())))){
                    notification(thing, category, place);
                }
                else {System.out.println("no");}
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean checkIfAlreadyNotified(String id){
        Map<String,?> keys = alreadyNotified.getAll();
        boolean flag = false;
        for(Map.Entry<String,?> entry : keys.entrySet()) {
            if (entry.getKey().equals(id)) {
                System.out.println(entry.getKey());
                flag = true;
            }
        }
        return flag;
    }


    public String extractCity(String address){
        String addressSplitted[] = address.split(",");
        if (addressSplitted.length >= 2){
            int length = addressSplitted.length;
            int cityPos = length - 2;
            String city = addressSplitted[cityPos].replaceAll("\\d+", "");
            System.out.println(StringUtils.containsWhitespace(city));
            city = StringUtils.deleteWhitespace(city);
            return StringUtils.stripAccents(city);
        }
        else {
            return " ";
        }

    }

    private void notification(MObject thing, String categoryName, String place){
        System.out.println("yes");
        String notifInfo = getResources().getString(R.string.notifInfoText);
        String notifCategory = getResources().getString(R.string.notifCategoryText);
        String notifPlace = getResources().getString(R.string.notifPlaceText);
        int requestID = (int) System.currentTimeMillis();
        Intent notificationIntent = new Intent(MainActivity.this, MoreInfoActivity.class);
        notificationIntent.putExtra("id", thing.getId());
        PendingIntent contentIntent = PendingIntent.getActivity(this, requestID, notificationIntent, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.main_icon)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(notifInfo + thing.getObjectName() +  notifCategory + categoryName + notifPlace + place)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notifInfo + thing.getObjectName() +  notifCategory + categoryName + notifPlace + place))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat nM = NotificationManagerCompat.from(getApplicationContext());
        mBuilder.setContentIntent(contentIntent);
        nM.notify((int) System.currentTimeMillis(), mBuilder.build());
        SharedPreferences.Editor shared = alreadyNotified.edit();
        shared.putString(thing.getId(), "true");
        shared.commit();

    }




    @Override
    public void setToken(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            String token = obj.getString("token");
            SharedPreferences pref = this.getSharedPreferences("auth", MODE_PRIVATE);
            pref.edit().putString("token", token).apply();
            System.out.println(pref.getString("token", token));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void errorToken(){
        Toast.makeText(getApplicationContext(), getApplicationContext().
                getResources().getString(R.string.tokenSuccInfo), Toast.LENGTH_SHORT).show();

    }


}


