package com.example.dellxps7100.ztratyanalezy;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Patrik on 03.04.2018.
 */

public interface UserLogin {
    @POST("/node/express/lostandfound/login")
    Call<ResponseBody> login(@Body RequestBody input);
}
