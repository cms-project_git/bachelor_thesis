package com.example.dellxps7100.ztratyanalezy;

/**
 * Created by Patrik on 22.03.2018.
 * Interface, ktery implementuje funkci, ktera je implementovana v NewThingActivity a volana v
 * UploadImageThread pri chybe pri odesilani
 * fotky
 */

public interface UploadImageFailure {
    public void showImageErrorToast();
}
