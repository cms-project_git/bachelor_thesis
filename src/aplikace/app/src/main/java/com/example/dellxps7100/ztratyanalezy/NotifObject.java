package com.example.dellxps7100.ztratyanalezy;

/**
 * Created by Patrik on 02.05.2018.
 */

public class NotifObject {
    private String address;
    private String category;
    private String date;

    NotifObject(String address, String category){
        this.address = address;
        this.category = category;
        this.date = Long.toString(System.currentTimeMillis());
    }

}
