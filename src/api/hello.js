var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var connect = require('connect');
var path = require('path');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');

//mongoose setup

app.use(connect.cookieParser());
app.use(connect.logger('dev'));
app.use(connect.bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(connect.cookieParser());
app.use(connect.logger('dev'));
app.use(connect.bodyParser());

app.use(connect.json());
app.use(connect.urlencoded());

app.use(connect.json());
app.use(connect.urlencoded());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
mongoose.connect('mongodb://admin:admin@ds217349.mlab.com:17349/lostandfound');
mongoose.Promise = global.Promise;

var port = process.env.PORT  || 43;

var thingSchema = new mongoose.Schema({
    objectName: {type: String, required: true},
    date: {type: String, required: true},
    category: {type: String, required: true},
    place: {type: String, required: true}, //toto bude taky required, ale az to nebude v emulatoru
    description: {type: String, required: true},
    contact: {type: String, required: true},
    password: {type: String,required: true},
    type: {type: String, required: true},
    insertTime : {type: String},
    versionKey: false
});

var userSchema = new mongoose.Schema({
    userName: {type: String, required: true},
    password: {type: String, required: true},
});


//model setup, collection things
var Thing = mongoose.model("thing", thingSchema) //not semicolon!!
var User = mongoose.model("user", userSchema)

////////////////////////////// AUTENFIKACE /////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

function checkToken(req, res, next){
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else {
        //Pristup odepren
        res.send(403);
    }
}



//zkouska jestli funguje autentifikace
app.get('/node/express/lostandfound/', function (req,res) {
    res.send("Welcome to Lost and found API.");
});


//pred ulozenim hesla ho zahashuji
userSchema.pre('save', function (next) {
    if (this.password){
        const salt = 10;
        this.password = bcrypt.hashSync(this.password, salt);
    }
    next();
});

thingSchema.pre('save', function (next) {
    if (this.password){
        const salt = 10;
        this.password = bcrypt.hashSync(this.password, salt);
    }
    var date = new Date();
    this.insertTime = date.getTime().toString();
    next();
});


//vraceni vsech uzivatelu
app.get('/node/express/lostandfound/all', checkToken, function (req, res){
    jwt.verify(req.token, 'lostandfound', (err, data)=>{
        if (err){
            res.status(403);
        }
        else{
            User.find({}, function (err, docs) {
            res.send(docs);
        });
}
});

});


//pridani noveho uzivatele
app.post("/node/express/lostandfound/new_user", checkToken, function (req, res){
    jwt.verify(req.token, 'lostandfound', (err, data)=> {
        if(!err)
    {
        var valid = checkInput(req.body);
        console.log(valid);
        if (valid.length != 0){
            res.status(400).send(valid);
            res.end();
        }
        var myData = new User(req.body);
        myData.save()
            .then(item => {
            res.send(item._id);
    })
    .catch(err => {
        res.send(err.toString());
    });
    }
else res.send(403);
});
});



//Funkce, ktera pro zaregistrovaneho uzivatele vrati token se kterym muze vyuzivat sluzby api
app.post('/node/express/lostandfound/login', function (req, serverResponse){
    User.find({userName: req.body.userName}, function (err, res) {
        if (res.length) {
            const result = bcrypt.compareSync(req.body.password, res[0].password);
            if (result) {
                jwt.sign("user", 'lostandfound', function (err, token) {
                    serverResponse.json({
                        token
                    });
                });
            } else {
                serverResponse.status(400).send("Bad password");
            }
        }
        else {
            serverResponse.status(400).send("User not found");
        }
    });
});



////////////////////////////// LOGIKA LOST AND FOUND /////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.post("/node/express/lostandfound/upload", checkToken, function (req, res){
    jwt.verify(req.token, 'lostandfound', (err, data)=> {
        if (!err){
        var myData = new Thing(req.body);
        myData.save()
            .then(item => {
            res.send(item._id);
    })
    .catch(err => {
            var notValidFields = checkInput(err);
        if (notValidFields.length != 0) {
            res.status(404).json(notValidFields);
        }
        else res.status(400);
    });
    } else res.status(403);
});
});


function checkInput(err){
    var json = {};
    if (err.name == 'ValidationError') {
        for (field in err.errors) {
            json[field] = false;
        }
    }
    return json;
}



app.get("/node/express/lostandfound/by-category",checkToken, function (req, res){
    jwt.verify(req.token, 'lostandfound', (err, data)=> {
        console.log(req.query.param);
    if(!err)
    {
        Thing.find({category: req.query.param}, function (err, docs) {
            res.send(docs);
        });
    } else  res.status(403);
});
});


app.get("/node/express/lostandfound/by-type", checkToken, function (req, res){
    jwt.verify(req.token, 'lostandfound', (err, data)=> {
        if(!err)
    {
        Thing.find({type: req.query.param}, function (err, docs) {
            res.send(docs);
        });
    } else  res.status(403);
});
});



function ifExistsRemove(filePath){
    fs.stat(filePath, function (err, res) {
        if (err == null){
            fs.unlink(filePath);
        }
    })
}

app.get("/node/express/lostandfound/delete", checkToken, function (req, res){
    jwt.verify(req.token, 'lostandfound', (errToken, dataToken)=> {
        if(!errToken)
    {
        Thing.findOne({_id: req.query.id}, function (errFind, docsFind) {
            if(docsFind) {
                var pass = docsFind.password;
                const result = bcrypt.compareSync(req.query.password, pass);
                if (result) {
                    Thing.remove({_id: req.query.id}, function (err, docs) {
                        if (docs) {
                            var path1 = "images/" + req.query.id + "0.jpg";
                            var path2 = "images/" + req.query.id + "1.jpg";
                            var path3 = "images/" + req.query.id + "2.jpg";
                            ifExistsRemove(path1);
                            ifExistsRemove(path2);
                            ifExistsRemove(path3);
                            res.send(200);
                        }
                        else res.send(400);
                    })
                }
                else res.send(400);
            }
            else res.send(400);
        })

    }else {
        res.send(403);
    }
});
});



app.get("/node/express/lostandfound/thing-by-id", function (req, res){
    Thing.find({_id: req.query.id}, function (err, docs) {
        res.send(docs);
    });
});

/* Nezapomenout vlozit index typu text do databaze */
app.get("/node/express/lostandfound/search", checkToken, function (req, res) {
    jwt.verify(req.token, 'lostandfound', (errToken, dataToken)=> {
        if(!errToken)
    {
        var param = req.query.param;
        var regex = new RegExp(".*" + param + ".*", "i");
        Thing.find({objectName: {$regex: regex}}, function (err, docs) {
            res.send(docs);
        });
    }
else res.send(403);
});
});


//image upload
app.post('/node/express/lostandfound/upload-image', checkToken, function(req, res) {
    jwt.verify(req.token, 'lostandfound', (errToken, dataToken)=> {
        if(!errToken)
    {
        fs.readFile(req.files.image.path, function (err, data) {
            var newPath = "images/" + req.files.image.originalFilename;
            console.log(newPath);
            fs.writeFile(newPath, data, function (err) {
                if (err) {
                    res.status(400);
                } else {
                    res.json({'response': "Saved"});
                }
            });
        });
    }
else  res.status(403);
});
});

app.get('/node/express/lostandfound/images/:file', function (req, res){
    try {
        file = req.params.file;
        var img = fs.readFileSync("images/" + file);
        res.writeHead(200, {'Content-Type': 'image/jpg' });
        res.end(img, 'binary');
    }
    catch (error)
    {res.send(404)}


});

app.get('/node/express/lostandfound/checkImage', function (req, res){
    try {
        file = req.query.filename;
        var img = fs.readFileSync("images/" + file);
        res.send(200);
    }
    catch (error)
    {res.send(404)}

});

app.get("/node/express/lostandfound/notification",checkToken, function (req, res){
    var output = new Array();
    jwt.verify(req.token, 'lostandfound', (err, data)=> {
        console.log(req.query.param);
    if(!err)
    {
        Thing.find({category: req.query.category}, function (err, docs) {
	    docs.forEach(function(element){
		if (element.insertTime > req.query.date) output.push(element);
	    });	
            res.send(output);
        });
    } else  res.status(403);
});
});












//nastaveni portu a zapnuti serveru
app.listen(333, '127.0.0.1');
console.log('Running');