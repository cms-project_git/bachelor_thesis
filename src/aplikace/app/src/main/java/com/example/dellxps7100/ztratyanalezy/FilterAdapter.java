package com.example.dellxps7100.ztratyanalezy;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Patrik on 29.03.2018.
 */

public class FilterAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> categories;
    FilterActivity activity;

    public FilterAdapter(Context context, FilterActivity activity) {
        this.context = context;
        this.activity = activity;
        categories = new ArrayList<>(Arrays.asList(context.getResources().getStringArray(R.array.categoriesArray)));
        categories.add("Ztráta");
        categories.add("Nález");
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final Button button;
        final String buttonText = categories.get(i);
        button = new Button(context);
        int backg = ContextCompat.getColor(context, R.color.menuBackground);
        button.setBackgroundColor(backg);
        button.setTextColor(Color.WHITE);
        button.setLayoutParams(new ViewGroup.LayoutParams(150, 30));
        button.setText(buttonText);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String types[] = activity.getResources().getStringArray(R.array.typeArray);
                if (Arrays.asList(types).contains(buttonText)) {
                    GetRequest request = new GetRequest(activity, button.getText().toString(), "type");
                    request.makeGetRequest();
                } else {
                    GetRequest request = new GetRequest(activity, button.getText().toString(), "category");
                    request.makeGetRequest();
                }

            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        button.setLayoutParams(params);

        return button;
    }


}
