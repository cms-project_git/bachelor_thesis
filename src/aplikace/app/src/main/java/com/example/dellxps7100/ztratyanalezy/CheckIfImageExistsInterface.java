package com.example.dellxps7100.ztratyanalezy;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.Query;

/**
 * Created by Patrik on 30.04.2018.
 */

public interface CheckIfImageExistsInterface {
    @GET("checkImage")
    Call<ResponseBody> checkIfImageExists(@Query("filename") String filename);
}
