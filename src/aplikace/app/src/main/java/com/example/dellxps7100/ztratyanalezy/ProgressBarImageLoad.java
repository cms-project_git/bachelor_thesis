package com.example.dellxps7100.ztratyanalezy;

import android.view.View;
import android.widget.ProgressBar;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

/**
 * Created by Patrik on 27.03.2018.
 * Trida, ktera ma slot progressBar, coz je klasicky android ProgressBar, ktery znazornuje nacitani
 * obrazku. Pro pouziti se v Glide zavola v .listener(ProgressBarImageLoad)
 */

public class ProgressBarImageLoad implements RequestListener<String, GlideDrawable> {
    ProgressBar progressBar;

    ProgressBarImageLoad(ProgressBar inputProgress){
        this.progressBar = inputProgress;
    }

    @Override
    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

        progressBar.setVisibility(View.GONE);
        return false;
    }

    @Override
    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

        progressBar.setVisibility(View.GONE);
        return false;
    }
}
