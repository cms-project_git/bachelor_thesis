package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.koushikdutta.async.http.NameValuePair;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 06.03.2018.
 */

public class SearchThing {
    private String inputString = "";
    private SearchThingActivity activity;

    SearchThing(String inputString, SearchThingActivity activity){
        this.inputString = inputString;
        this.activity = activity;
    }

    public void searchThing(){
        ServerInfo server = new ServerInfo();

        AuthInterceptor auth = new AuthInterceptor(activity);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();

        SearchThingInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(SearchThingInterface.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.searchThing(this.inputString);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.code() == 200){
                    try {
                        activity.renderFoundThings(response.body().string());
                    } catch (IOException e) {
                        activity.searchFailure();
                    }
                }
                else activity.searchFailure();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                activity.searchFailure();
            }
        });

    }



 }
