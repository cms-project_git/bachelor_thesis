
A) Kompilace v�sledn�ho instala�n�ho souboru 
--------------------------------------------
Pro z�sk�n� instala�n�ho souboru z projektu je pot�eba na��st ve�ker� soubory ze slo�ky, kter� se jmenuje src/aplikace/, do Android Studia a pot� pomoc� n�j exportovat v�sledn� instala�n� soubor. 


B) Instalace Android aplikace 

--------------------------------------------
Pro zprovozn�n� aplikace na mobiln�m za��zen� je pot�eba do za��zen� p�esunout soubor s p��ponou .apk, kter� je ve slo�ce app/ a ten nainstalovat. Po instalaci je aplikace p�ipravena k pou�it�. 

C) Spu�t�n� API 
--------------------------------------------
Server lze spustit na Windows Server 2012 a to pomoc� vestav�n�ho IIS. Do t�to slu�by je pot�eba doinstalovat knihovnu IISnode, kter� je dostupn� z https://github.com/tjanczuk/iisnode. Po instalaci t�to knihovny je pot�eba spustitsouborsetupsamples.bat.
Pot� se ve Spr�vci internetov� informa�n� slu�by a v polo�ce Weby zobraz� polo�ka node. Jako posledn� krok do slo�ky express p�em�st�me cel� obsah slo�ky se serverem a v souboru web.con?g v m�st� <action type="Rewrite"url="hello.js"/> nastav�me URL na soubor s p��ponou .js s na��m serverem. 

D) Odkaz na API
--------------------------------------------
API je zprovozn�no na za��zen� s opera�n�m syst�mem WindowsServer a b�� na �koln�m serveru s adresou https://student.inf.upol.cz/node/express/lostandfound. 