package com.example.dellxps7100.ztratyanalezy;

import android.content.SharedPreferences;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;



public class PostRequestJson {
    JSONObject uploadedJson = null;
    String url = "";
    NewThingActivity activity;

    PostRequestJson(String url, JSONObject uploadedObject, NewThingActivity activity) {
        this.uploadedJson = uploadedObject;
        this.url = url;
        this.activity = activity;

    }

    public void uploadThing() {
        ServerInfo server = new ServerInfo();
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                this.uploadedJson.toString());
        AuthInterceptor auth = new AuthInterceptor(activity);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();

        PostThingInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(PostThingInterface.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.postThing(body);
        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                //pokud vse probehlo v poradku
                if (response.code() == 200) {
                    try {
                        activity.showSuccessThingMessage(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //pokud nejsou vyplnena nektera z povinnych poli
                } else if (response.code() == 404) {
                    try {
                        String json = response.errorBody().string();
                        JSONObject obj = new JSONObject(json);
                        activity.notValidInput(obj);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    activity.showThingErrorToast();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                activity.showThingErrorToast();
            }
        });

    }

}




