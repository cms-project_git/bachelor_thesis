package com.example.dellxps7100.ztratyanalezy;

/**
 * Created by Patrik on 13.02.2018.
 */

public final class ServerInfo {
    String URL = "https://student.inf.upol.cz/node/express/lostandfound/";
    String PORT = "8080";

    public String getServerAddress(){
        return URL;
    }

    public String getServerImagesFolder(){
        return URL + "images/";
    }
}
