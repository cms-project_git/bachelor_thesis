package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;
import android.content.SharedPreferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Patrik on 03.04.2018.
 */

public class AuthInterceptor implements Interceptor{

    Activity activity;

    AuthInterceptor(Activity activity){
        this.activity = activity;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        SharedPreferences pref = activity.getSharedPreferences("auth", MODE_PRIVATE);
        String token = pref.getString("token", null);
        Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
        return chain.proceed(request);
    }
}
