package com.example.dellxps7100.ztratyanalezy;
import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import javax.crypto.SecretKeyFactory;

import in.myinnos.awesomeimagepicker.activities.AlbumSelectActivity;
import in.myinnos.awesomeimagepicker.helpers.ConstantsCustomGallery;
import retrofit2.http.POST;


/**
 * Created by Dell XPS 7100 on 18.01.2018.
 */

public class NewThingActivity extends AppCompatActivity implements UploadImageFailure, UploadThingFailure, UploadThingSuccess {
    private static final int PICK_IMAGE = 1;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;
    private int STORAGE_ACCESS_PERMS = 3;
    private static ArrayList<String> imgPaths = new ArrayList<>();
    private static Context context;
    private String[] jsonAttributes = new String[]{"objectName", "date", "contact", "description", "password"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_thing);
        MenuListener menuListener = new MenuListener();
        menuListener.listen(this.findViewById(android.R.id.content), getApplicationContext());
        this.context = getApplicationContext();
        controllersCaller();
        getPerms();
    }

     private void controllersCaller(){
        googleMapPicker();
        callDatePicker();
        callTypePicker();
        callCategoryPicker();
        uploadThingController();
        uploadImgController();
    }


    public void googleMapPicker(){
        final TextView place = (TextView) findViewById(R.id.placeText);
        place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleAutoCompleteStart();
            }
        });
    }

    private void googleAutoCompleteStart(){
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(NewThingActivity.this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Toast.makeText(getApplicationContext(), getResources()
                    .getString(R.string.googleMapsError), Toast.LENGTH_LONG).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getApplicationContext(), getResources()
                    .getString(R.string.googleMapsError), Toast.LENGTH_LONG).show();
        }
    }

    public void getPerms(){
        String perms[] = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if (ContextCompat.checkSelfPermission(this, perms[0]) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, perms, STORAGE_ACCESS_PERMS);
        }
    }

    private void googlePlaceWasPick(Intent data){
        Place place = PlaceAutocomplete.getPlace(this, data);
        final TextView placeText = (TextView) findViewById(R.id.placeText);
        placeText.setText(place.getAddress().toString());
        Log.i("MAPA", "Place: " + place.getName());
    }

    private void uploadThingController() {
        final ServerInfo server = new ServerInfo();
        FloatingActionButton up = (FloatingActionButton) findViewById(R.id.uploadButton);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    PostRequestJson request = new PostRequestJson(server.getServerAddress(), getNewData(), NewThingActivity.this);
                    request.uploadThing();
            }
        });
    }

    private void makeMultipleImageUpload(String id){
        String cuttenId = id.substring(1, id.length() - 1); //musim odstranit " a "
        for(int i = 0; i < imgPaths.size(); i++){
           UploadImageThread thread = new UploadImageThread(cuttenId + i, imgPaths.get(i), this);
           try {
               thread.run();
           }
           catch (Exception e){
               Toast.makeText(this, getResources().getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
           }
        }
        Toast.makeText(this, getResources().getString(R.string.successSentMessage), Toast.LENGTH_SHORT).show();
        resetForm();
        imgPaths = new ArrayList<>();
    }

    private void resetForm(){
        TextView mapPlace = (TextView) findViewById(R.id.placeText);
        for (String simpleOne : jsonAttributes) {
            String id = simpleOne + "EditText";
            int editTextID = getResources().getIdentifier(id, "id", getPackageName());
            EditText actualEditText = (EditText) findViewById(editTextID);
            actualEditText.setText("");
        }
        mapPlace.setText("");
        hideWarningCard(getLayoutInflater().inflate(R.layout.input_warning, null));
        hideWarningCard(getLayoutInflater().inflate(R.layout.image_success_warning, null));
        removeImagesFromView();
    }

    private void uploadImgController() {
        FloatingActionButton imgUp = (FloatingActionButton) findViewById(R.id.uploadImageButton);
        imgUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent (getApplicationContext(), AlbumSelectActivity.class);
                galleryIntent.putExtra(ConstantsCustomGallery.INTENT_EXTRA_LIMIT, 3);
                startActivityForResult(galleryIntent, PICK_IMAGE);
            }
        });


    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (resCode == RESULT_OK && reqCode == PICK_IMAGE) {
            ArrayList<in.myinnos.awesomeimagepicker.models.Image> imgs
                    = data.getParcelableArrayListExtra(ConstantsCustomGallery.INTENT_EXTRA_IMAGES);
            //nactu cesty obrazku do promenne tridy
            setPathsFromGallery(imgs);
            //obrazek byl uspesne vlozen, ukazu info
            showWarningCard(getLayoutInflater().inflate(R.layout.image_success_warning, null));
        }

        if (reqCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resCode == RESULT_OK) {
                googlePlaceWasPick(data);
            } else if (resCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resCode == RESULT_CANCELED) {}
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == STORAGE_ACCESS_PERMS){
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                this.finish();
                System.exit(0);
            }
        }
    }


    //funkce, ktera z galerie obrazku prida cesty do promenne tridy
    private void setPathsFromGallery(ArrayList<in.myinnos.awesomeimagepicker.models.Image> imgs) {
        showWarningCard(getLayoutInflater().inflate(R.layout.images_strip, null));
        ArrayList<ImageView> views = new ArrayList<>();
        views.add((ImageView) findViewById(R.id.firstThingImage));
        views.add((ImageView) findViewById(R.id.secondThingImage));
        views.add((ImageView) findViewById(R.id.thirdThingImage));
        for (int i = 0; i < imgs.size(); i++) {
            String path = imgs.get(i).path;
            imgPaths.add(path);
            views.get(i).setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(path)
                    .override(800,600)
                    .into(views.get(i));
        }
    }

    private void removeImagesFromView(){
        ArrayList<ImageView> views = new ArrayList<>();
        views.add((ImageView) findViewById(R.id.firstThingImage));
        views.add((ImageView) findViewById(R.id.secondThingImage));
        views.add((ImageView) findViewById(R.id.thirdThingImage));
        for (ImageView imageView : views) {
            if (imageView != null) imageView.setVisibility(View.GONE);
        }
    }

    public JSONObject getNewData() {
        JSONObject newDataObject = new JSONObject();
        boolean validFlag = true;
        for (String simpleOne : jsonAttributes) {
            String id = simpleOne + "EditText";
            int editTextID = getResources().getIdentifier(id, "id", getPackageName());
            EditText actualEditText = (EditText) findViewById(editTextID);
            try {
                newDataObject.put(simpleOne, actualEditText.getText());
            } catch (JSONException e) {
                e.printStackTrace();
                System.out.println("Error in NewThingActivity.java - adding to new JSON");
            } catch (Exception e) {
                System.out.println("Error in NewThingActivity.java");
            }
        }
        if (!validFlag) return null; //pokud se nasel nejaky nevyplneny (povinny) input uz skoncim a nevkladam
        Spinner category = (Spinner) findViewById(R.id.categoriesSpinner);
        Spinner type = (Spinner) findViewById(R.id.typeSpinner);
        TextView placeText = (TextView) findViewById(R.id.placeText);
        try {
            newDataObject.put("category", category.getSelectedItem().toString());
            newDataObject.put("type", type.getSelectedItem().toString());
            newDataObject.put("place", placeText.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
            System.out.println("Error in NewThingActivity.java - adding to new JSON");
        } catch (Exception e) {
            System.out.println("Error in NewThingActivity.java");
        }
        return newDataObject;
    }

    protected void showWarningCard(View inputView){
        RelativeLayout inputForm = (RelativeLayout) findViewById(R.id.warningContainer);
        ScrollView scroller = (ScrollView) findViewById(R.id.newThingScrollView);
        inputForm.addView(inputView);
        scroller.scrollTo(0, inputForm.getBottom());
    }

    protected void hideWarningCard(View inputView){
        RelativeLayout inputForm = (RelativeLayout) findViewById(R.id.warningContainer);
        ScrollView scroller = (ScrollView) findViewById(R.id.newThingScrollView);
        inputForm.removeView(scroller);
    }


    private void callTypePicker() {
        Spinner spin = (Spinner) findViewById(R.id.typeSpinner);
        ArrayAdapter<CharSequence> types = ArrayAdapter.createFromResource(this,
                R.array.typeArray, android.R.layout.simple_spinner_item);
        types.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(types);
    }

    public void callCategoryPicker() {
        Spinner spin = (Spinner) findViewById(R.id.categoriesSpinner);
        ArrayAdapter<CharSequence> types = ArrayAdapter.createFromResource(this,
                R.array.categoriesArray, android.R.layout.simple_spinner_item);
        types.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(types);
    }


    private void callDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        CardView cardDate = (CardView) findViewById(R.id.dateCard);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                calendar.set(Calendar.YEAR, y);
                calendar.set(Calendar.MONTH, m);
                calendar.set(Calendar.DAY_OF_MONTH, d);
                setDateText(calendar);

            }
        };

        EditText datePick = (EditText) findViewById(R.id.dateEditText);
        datePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(NewThingActivity.this, date, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        cardDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(NewThingActivity.this, date, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


    }

    private void setDateText(Calendar calendar) {
        String format = "dd.MM.yyyy";
        SimpleDateFormat date = new SimpleDateFormat(format, Locale.UK);
        EditText datePick = (EditText) findViewById(R.id.dateEditText);
        datePick.setText(date.format(calendar.getTime()));
    }

    /*
        Pokud nejsou vyplnena vsechna povinna pole, zobrazim nahore info
        Nactu si layout, ktery pak vlozim do containeru
     */

    public void notValidInput(JSONObject json) { //testuju jestli jsou povinna pole vyplnena
        ArrayList<String> required = extractFieldNames(json);
        ArrayList<String> notEditText = new ArrayList<>(Arrays.asList("type", "category", "place"));
        TextView mapPlace = (TextView) findViewById(R.id.placeText);
        for (String simpleOne : required) {
            if (required.contains("place")){
                mapPlace.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_error_24dp, 0);
            }
            if (!notEditText.contains(simpleOne)) {
                String id = simpleOne + "EditText";
                int editTextID = getResources().getIdentifier(id, "id", getPackageName());
                EditText actualEditText = (EditText) findViewById(editTextID);
                actualEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_error_24dp, 0);
            }
        }
        showWarningCard(getLayoutInflater().inflate(R.layout.input_warning, null));
    }

    //API pri nevyplneni povinnych dat vrati JSON s nazvy, zde tyto jmena extrahuji z JSONu do Stringu
    public ArrayList<String> extractFieldNames(JSONObject input){
        ArrayList<String> output = new ArrayList<>();
        Iterator<?> keys = input.keys();
        while (keys.hasNext()){
           output.add(keys.next().toString());
        }
        return output;
    }


    /* Implementace metody, ktera je volana pri chybe nahravani obrazku */
    @Override
    public void showImageErrorToast() {
        Toast.makeText(this, getResources().getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showThingErrorToast(){
        Toast.makeText(this, getResources().getString(R.string.errorSentMessage), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSuccessThingMessage(String id) {
        makeMultipleImageUpload(id);
    }
}
