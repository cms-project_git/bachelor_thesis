package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.logging.Filter;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 03.04.2018.
 */

public class GetRequest {


    Activity activity;
    String param;
    String type;
    String key;

    GetRequest(Activity activity, String param, String type){
        this.activity = activity;
        this.param = param;
        this.type = type;
    }

    public void makeGetRequest() {
        ServerInfo server = new ServerInfo();

        AuthInterceptor auth = new AuthInterceptor(activity);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();

        if (type.equals("type")){
            GetRequestTypeInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                    .client(client).build().create(GetRequestTypeInterface.class);

            final retrofit2.Call<okhttp3.ResponseBody> request = service.getByType(this.param);
            request.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        try {
                            if (activity instanceof FilterActivity){
                                 FilterActivity act = (FilterActivity) activity;
                                 act.filter(response.body().string(), param);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        if (activity instanceof FilterActivity){
                            FilterActivity act = (FilterActivity) activity;
                            act.filterFailure();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
        else if(type.equals("category")){
            GetRequestCategoryInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                    .client(client).build().create(GetRequestCategoryInterface.class);

            final retrofit2.Call<okhttp3.ResponseBody> request = service.getByCategory(this.param);
            System.out.println(request.request().url());
            request.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        try {
                            if (activity instanceof FilterActivity){
                                FilterActivity act = (FilterActivity) activity;
                                act.filter(response.body().string(), param);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        if (activity instanceof FilterActivity){
                            FilterActivity act = (FilterActivity) activity;
                            act.filterFailure();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (activity instanceof FilterActivity){
                        FilterActivity act = (FilterActivity) activity;
                        act.filterFailure();
                    }
                }
            });
        }
    }
}
