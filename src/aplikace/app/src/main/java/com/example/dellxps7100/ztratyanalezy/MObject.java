package com.example.dellxps7100.ztratyanalezy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell XPS 7100 on 25.11.2017.
 */

public class MObject {

    private String id = null;
    private String objectName = null;
    private String date = null;
    private String category = null;
    private String place = null;
    private String description = null;
    private String contact = null;
    private String type = null;
    private String password = null;
    private String img = null;


    public static ArrayList<MObject> getResponseList(JSONArray input){
        ArrayList<MObject> responseList = new ArrayList<>();
        try {
            JSONObject jsonObject = input.getJSONObject(0);
            for (int i = 0; i < input.length(); i++){
                jsonObject = input.getJSONObject(i);
                MObject temp = new MObject();
                temp.obJsonToMObj(jsonObject);
                responseList.add(temp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return responseList;
    }


    public void obJsonToMObj(JSONObject inObj) { // vytvori mi z JSONu reprezentaci meho objektu
        try {
            //MUST BE CHANGE WHEN DATABASE STRUCTURE WAS CHANGED!
            this.setId(inObj.getString("_id"));
            this.setObjectName(inObj.getString("objectName"));
            this.setDate(inObj.getString("date"));
            this.setCategory(inObj.getString("category"));
            this.setPlace(inObj.getString("place"));
            this.setDescription(inObj.getString("description"));
            this.setContact(inObj.getString("contact"));
            this.setType(inObj.getString("type"));
            this.setPassword(inObj.getString("password"));
        } catch (JSONException e) {
            System.out.println("Method obJsonToMObj error");
        }
    }

    @Override
    public String toString() {
        String output = "_id:" + this.getId() + " object-name:" + this.getObjectName() + " date:" +
                this.getDate() + " category:" + this.getCategory() + " place:" + this.getPlace() + " description:" + this.getDescription()
                + " contanct:" + this.getContact() + "type" + this.getType() + " password:" + this.getPassword();
        return output;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContact() {
        return contact;
    }


    public void setContact(String contact) {
        this.contact = contact;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
