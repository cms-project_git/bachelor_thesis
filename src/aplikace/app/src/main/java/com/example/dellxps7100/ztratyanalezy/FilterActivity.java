package com.example.dellxps7100.ztratyanalezy;


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;

public class FilterActivity extends AppCompatActivity {
    private ListView listOfThings = null; // promenne pro vytvoreni seznamu veci z httprequestu
    private ArrayList<MObject> rowItemsList = new ArrayList<>();
    private int LOCATION_PERMS_CODE = 1;
    private Intent actualState;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        GridView filterGrid = (GridView) findViewById(R.id.filterGrid);
        filterGrid.setAdapter(new FilterAdapter(getApplicationContext(), this));
        MenuListener menuListener = new MenuListener();
        menuListener.listen(this.findViewById(android.R.id.content), getApplicationContext());
        getPerms();
    }

    public void onRestart(){
        super.onRestart();
        startActivity(getIntent());
    }

     public void getPerms(){
        String perms[] = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if ((ContextCompat.checkSelfPermission(this, perms[0]) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, perms[1]) != PackageManager.PERMISSION_GRANTED)){
            ActivityCompat.requestPermissions(this, perms, LOCATION_PERMS_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        if (requestCode == LOCATION_PERMS_CODE){

            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                this.finish();
                System.exit(0);
            }
        }

    }

    protected void clickedItemController(int indexInput) {
        switchToMoreInfoItemActivity(indexInput);
    }


    // switch to MoreInfo activity with intent which contains property values of clicked item
    protected void switchToMoreInfoItemActivity(int indexOfItem) {
        MObject item = this.rowItemsList.get(indexOfItem);
        this.actualState = getIntent();
        String idOfItem = item.getId();
        Intent intent = new Intent(FilterActivity.this, MoreInfoActivity.class);
        intent.putExtra("id", idOfItem);
        startActivity(intent);
    }

    public void filter(String input, String param) {
        System.out.println(input);
        JSONArray json = null;
        try {
            json = new JSONArray(input);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        TextView filterText = (TextView) findViewById(R.id.showedThingsTextView);
        String baseText = getApplicationContext().getResources().getString(R.string.showedThingsText);
        filterText.setText(baseText + " " + param);
        renderResponse(json);
    }

    private void renderResponse(JSONArray json){
        this.rowItemsList = MObject.getResponseList(json);
        ThingsAdapter adptr = new ThingsAdapter(FilterActivity.this, this.rowItemsList, this);
        this.listOfThings = (ListView) findViewById(R.id.lvLostThings);
        this.listOfThings.setAdapter(adptr);
    }

    public void filterFailure(){
        Toast.makeText(getApplicationContext(),
                getApplicationContext().getResources().getString(R.string.noConnectionWarning), Toast.LENGTH_SHORT).show();
    }

}
