package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 03.04.2018.
 */

public class Login {

    MainActivity activity;
    String userName;
    String password;

    Login (String userName, String password, MainActivity activity){
        this.activity = activity;
        this.userName = userName;
        this.password = password;
    }

    public JSONObject makeJsonFromCredentials(){
        JSONObject user = new JSONObject();
        try {
            user.put("userName", userName);
            user.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;

    }

    public void makeLogin(){
        ServerInfo server = new ServerInfo();
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                makeJsonFromCredentials().toString());
        OkHttpClient client = new OkHttpClient.Builder().build();

        UserLogin service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(UserLogin.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.login(body);

        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    try {
                        activity.setToken(response.body().string());
                    }
                    catch (NullPointerException e){
                        activity.errorToken();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                activity.errorToken();
            }
        });
    }


}
