package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by Dell XPS 7100 on 10.12.2017.
 */

public class ThingsAdapter extends BaseAdapter {

    private static ArrayList<MObject> row;
    private LayoutInflater inflater;
    private Context inContext = null;
    private Activity act = null;
    public ThingsAdapter(Context context, ArrayList<MObject> inputList, Activity act) {
        this.row = inputList;
        this.inflater = LayoutInflater.from(context);
        this.inContext = context;
        this.act = act;
    }


    @Override
    public int getCount() {
        return row.size();
    }

    @Override
    public Object getItem(int i) {
        return row.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public ArrayList<MObject> getRow() {
        return this.row;
    }

    public Context getInContext() {
        return inContext;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        view = this.inflater.inflate(R.layout.thing_row, viewGroup, false);
        final ImageView thingImg = (ImageView) view.findViewById(R.id.thingImg);
        TextView thingTitle = (TextView) view.findViewById(R.id.thingTitle);
        TextView thingDate = (TextView) view.findViewById(R.id.thingDate);
        TextView type = (TextView) view.findViewById(R.id.typeTextView);
        CardView infoCard = (CardView) view.findViewById(R.id.itemViewCard);
        //z duvodu android bugu musim rucne menit barvu karty
        infoCard.setBackgroundColor(Color.WHITE);

        final MObject item = (MObject) getItem(i);
        thingTitle.setText(item.getObjectName());
        thingDate.setText(item.getDate());
        type.setText(item.getType());
        ServerInfo server = new ServerInfo();
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);
        Glide.with(inContext)
                    //lze nahrat az 3 obrazky, ten s 0 za nazvem je hlavni a ten hledam pro zobrazeni
                    //ve vypisu veci
                    .load(server.getServerAddress() + "/images/" + item.getId() + "0.jpg")
                    .listener(new ProgressBarImageLoad((ProgressBar) view.findViewById(R.id.progress)))
                    .error(R.drawable.defaultimage)
                    .into(thingImg);

        LinearLayout itemLayout = (LinearLayout) view.findViewById(R.id.lostThingItem);

        itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (act instanceof SearchThingActivity) {
                    ((SearchThingActivity) act).clickedItemController(i);
                }
                else if (act instanceof FilterActivity){
                    ((FilterActivity) act).clickedItemController(i);
                }
            }
        });


        return view;
    }

}
