package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Patrik on 05.03.2018.
 */

public class SearchThingActivity extends Activity {
    private ListView listOfThings = null; // promenne pro vytvoreni seznamu veci z httprequestu
    private ArrayList<MObject> rowItemsList = new ArrayList<>();
    private String searchState = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_thing);
        MenuListener menuListener = new MenuListener(); //menu controll
        menuListener.listen(this.findViewById(android.R.id.content), getApplicationContext());
        searchEngine();
    }

    @Override
    public void onRestart(){
        super.onRestart();
        startActivity(getIntent());
    }

    public void searchEngine() {
        final android.support.v7.widget.SearchView search = (android.support.v7.widget.SearchView) findViewById(R.id.searchView);
        search.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchState = query;
                if (query.length() >= 3) return searchLogic(query);
                else return true;
            }

            @Override
            public boolean onQueryTextChange(final String query) {
                searchState = query;
                if (query.length() >= 3) return searchLogic(query);
                else return true;
            }
        });
    }


    protected boolean searchLogic(String query){
        SearchThing search = new SearchThing(query, this);
        search.searchThing();
        return true;
    }


    public void renderFoundThings(final String newText) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONArray jsonArray = new JSONArray(newText);
                    ArrayList<MObject> responseList = new ArrayList<>();
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        MObject temp = new MObject();
                        temp.obJsonToMObj(jsonObject);
                        responseList.add(temp);
                    }
                    rowItemsList = responseList;
                    ThingsAdapter adptr = new ThingsAdapter(SearchThingActivity.this, responseList, SearchThingActivity.this);
                    listOfThings = (ListView) findViewById(R.id.lvLostThingsSearched);
                    listOfThings.setAdapter(adptr);
                    adptr.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).run();

    }

    protected void clickedItemController(int indexInput) {
        switchToMoreInfoItemActivity(indexInput);
    }

    protected void switchToMoreInfoItemActivity(int indexOfItem) {
        MObject item = this.rowItemsList.get(indexOfItem);
        String idOfItem = item.getId();
        Intent intent = new Intent(SearchThingActivity.this, MoreInfoActivity.class);
        intent.putExtra("id", idOfItem);
        startActivity(intent);
    }

    public void searchFailure(){
        Toast.makeText(getApplicationContext(),
                getApplicationContext().getResources().getString(R.string.noConnectionWarning), Toast.LENGTH_SHORT).show();
    }

}
