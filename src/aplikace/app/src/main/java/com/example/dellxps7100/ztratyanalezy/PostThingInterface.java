package com.example.dellxps7100.ztratyanalezy;

import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Patrik on 27.03.2018.
 */

public interface PostThingInterface {
    @POST("upload")
    Call<ResponseBody> postThing(@Body RequestBody input);
}
