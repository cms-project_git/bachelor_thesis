package com.example.dellxps7100.ztratyanalezy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 03.04.2018.
 */

public class MoreInfo {

    MoreInfoActivity activity;
    String id;

    MoreInfo(MoreInfoActivity activity, String id){
        this.activity = activity;
        this.id = id;
    }

    public void getMoreInfo(){
        ServerInfo server = new ServerInfo();
        AuthInterceptor auth = new AuthInterceptor(activity);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();
        MoreInfoInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(MoreInfoInterface.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.thingById(this.id);

        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200){
                    try {
                        activity.renderViewOfThing(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else activity.connectionFail();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                activity.connectionFail();
            }
        });
    }
}
