package com.example.dellxps7100.ztratyanalezy;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Patrik on 04.05.2018.
 */

public interface NotifyInterface {
    @GET("notification")
    Call<ResponseBody> notify(@Query("category") String category, @Query("date") String date);
}
