package com.example.dellxps7100.ztratyanalezy;

import android.app.Activity;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 04.05.2018.
 */

public class Notification {
    Activity activity;
    String place;
    String category;
    String time;
    String key;

    Notification(Activity activity, String place, String category, String time, String key){
        this.activity = activity;
        this.place = place;
        this.category = category;
        this.time = time;
        this.key = key;
    }

    public void makeNotification(){
        ServerInfo server = new ServerInfo();

        AuthInterceptor auth = new AuthInterceptor(activity);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();

       NotifyInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(NotifyInterface.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.notify(this.category, this.time);

        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (activity instanceof MainActivity){
                    MainActivity act = (MainActivity) activity;
                    try {
                        act.checkIfNotify(response.body().string(), place, category, key);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.print("notif error");
            }
        });

    }



}
