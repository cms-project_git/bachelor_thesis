package com.example.dellxps7100.ztratyanalezy;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MoreInfoActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int PICK_IMAGE = 1;
    private static final int GOOGLE_MAPS_PICK = 2;
    private static MObject inputThing;
    private int addedImages = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info);
        MenuListener menuListener = new MenuListener();
        menuListener.listen(this.findViewById(android.R.id.content), getApplicationContext());
        getGeocodePerms();
        String id = getIntent().getStringExtra("id");
        MoreInfo moreInfoRequest = new MoreInfo(this, id);
        moreInfoRequest.getMoreInfo();
        CheckImageIfExists checkImageIfExists = new CheckImageIfExists(this, id + "1.jpg");
        checkImageIfExists.checkIfExists();
        CheckImageIfExists checkImageIfExistsSecond = new CheckImageIfExists(this, id + "2.jpg");
        checkImageIfExistsSecond.checkIfExists();



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[]grantResults){
        switch (requestCode){
            case GOOGLE_MAPS_PICK: {
                setGoogleMapFromDatabase(inputThing.getPlace());
            }
        }
    }

    public void noAdditionImages(){
        addedImages++;
        if (addedImages == 2){
            CardView imagesHolder = (CardView) findViewById(R.id.addedImages);
            imagesHolder.setVisibility(View.GONE);
        }
    }

   protected void deleteThingButtonController(final String id, final String pass){
        FloatingActionButton dltButt = (FloatingActionButton) findViewById(R.id.deleteItemButton);
        dltButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MoreInfoActivity.this);
                alertDialog.setTitle("Zadejte heslo");
                final EditText inputPass = new EditText(getApplicationContext());
                inputPass.setTextColor(getResources().getColor(R.color.black));
                alertDialog.setView(inputPass);
                alertDialog.setPositiveButton("SMAZAT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DeleteThing delete = new DeleteThing(MoreInfoActivity.this, id, inputPass.getText().toString());
                        delete.makeDelete();
                    }
                });

                alertDialog.setNegativeButton("ZRUŠIT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();

            }
        });
    }

    protected void renderViewOfThing(String response){
        MObject obj = new MObject();
        try {
            JSONArray arr = new JSONArray(response);
            JSONObject jsonObj = arr.getJSONObject(0);
            obj.obJsonToMObj(jsonObj);
            inputThing = obj;
            makeCardOfThing();
            setGoogleMapFromDatabase(inputThing.getPlace());
            deleteThingButtonController(inputThing.getId(), inputThing.getPassword());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    protected void makeCardOfThing(){
        TextView dateView = (TextView) findViewById(R.id.thingDate);
        TextView titleView = (TextView) findViewById(R.id.thingTitle);
        ImageView imgView = (ImageView) findViewById(R.id.thingImg);
        ImageView secondImage = (ImageView) findViewById(R.id.firstImageView);
        ImageView thirdImage = (ImageView) findViewById(R.id.secondImageView);
        TextView typeView = (TextView) findViewById(R.id.typeTextView);
        TextView descriptionText = (TextView) findViewById(R.id.descriptionTextView);
        TextView contactText = (TextView) findViewById(R.id.contactTextView);
        ServerInfo server = new ServerInfo();
        setImageToView(server.getServerImagesFolder(),  imgView, 0);
        setImageToView(server.getServerImagesFolder(), secondImage, 1);
        setImageToView(server.getServerImagesFolder(), thirdImage, 2);
        titleView.setText(inputThing.getObjectName());
        dateView.setText(inputThing.getDate());
        typeView.setText(inputThing.getType());
        descriptionText.setText(inputThing.getDescription());
        contactText.setText(inputThing.getContact());

    }

    private void setImageToView(String url, ImageView img, int imgIndex){
        Glide.with(getApplicationContext())
                .load(url + inputThing.getId() + imgIndex + ".jpg")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new ProgressBarImageLoad((ProgressBar) findViewById(R.id.progress)))
                .error(R.drawable.defaultimage)
                .into(img);
    }

    private void setGoogleMapFromDatabase(String place){
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.googleMapView);
        mapFragment.getMapAsync(MoreInfoActivity.this);
    }


    private void getGeocodePerms() {
        if (((ContextCompat.checkSelfPermission(MoreInfoActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)) &
                (ContextCompat.checkSelfPermission(MoreInfoActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)))
                != PackageManager.PERMISSION_GRANTED){

            String [] perms = {Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(MoreInfoActivity.this, perms, GOOGLE_MAPS_PICK);

        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        double lat = 0;
        double lng = 0;
        try {
            GeoApiContext context = new GeoApiContext.Builder()
                    .apiKey("AIzaSyA8DUnLotjHtCDQhMwKNvDT-G8YaMnLtbA")
                    .build();
            GeocodingResult[] result = null;
            result = GeocodingApi.geocode(context, inputThing.getPlace()).await();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JSONObject jsonLoc = null;
            jsonLoc = new JSONObject(gson.toJson(result[0].geometry));
            String location = jsonLoc.getString("location").toString();
            JSONObject jsonLocation = new JSONObject(location);
            lat = jsonLocation.getDouble("lat");
            lng = jsonLocation.getDouble("lng");

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.googleMapsError)
                    ,Toast.LENGTH_SHORT);
        }
        try {
            googleMap.setMyLocationEnabled(true);
            LatLng placeLatlng = new LatLng(lat, lng);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placeLatlng, 13));
            googleMap.addMarker(new MarkerOptions()
                    .title(inputThing.getPlace())
                    .position(placeLatlng));
        }
        catch (SecurityException e){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.googleMapsError)
                    ,Toast.LENGTH_SHORT);
        }
     }

     public void succDelete(){
        Toast.makeText(getApplicationContext(),
                getResources().getString(R.string.succDeleteText), Toast.LENGTH_SHORT).show();
         finish();
     }

     public void failDelete(){
         Toast.makeText(getApplicationContext(),
                 getResources().getString(R.string.failDeleteText), Toast.LENGTH_SHORT).show();
     }

     public void connectionFail(){
         Toast.makeText(getApplicationContext(),
                 getApplicationContext().getResources().getString(R.string.noConnectionWarning), Toast.LENGTH_SHORT).show();
     }
}
