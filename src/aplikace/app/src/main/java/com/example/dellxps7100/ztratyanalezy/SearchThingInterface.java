package com.example.dellxps7100.ztratyanalezy;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Patrik on 05.04.2018.
 */

public interface SearchThingInterface {
    @GET("search")
    Call<ResponseBody> searchThing(@Query("param") String param);
}
