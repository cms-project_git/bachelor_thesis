package com.example.dellxps7100.ztratyanalezy;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Patrik on 03.04.2018.
 */

public interface MoreInfoInterface {
    @GET("thing-by-id")
    Call<ResponseBody> thingById(@Query("id") String id);
}
