package com.example.dellxps7100.ztratyanalezy;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Patrik on 20.03.2018.
 * Interface, ktere pouziva knihovnu Retrofit a predepisuje funkci pro upload obrazku na server.
 * Je pouzivano v NewThingActivity.
 */

public interface UploadImageInterface {
        @Multipart
        @POST("upload-image")
        Call<ResponseBody> postImage(@Part MultipartBody.Part image, @Part("name") RequestBody name);
}
