package com.example.dellxps7100.ztratyanalezy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;

/**
 * MenuListener
 * Created by Patrik on 15.02.2018.
 */

public class MenuListener {


    public void listen(final View view, final Context context) {
        increaseIconSize(view, context);
        BottomNavigationView menu = (BottomNavigationView) view.findViewById(R.id.bottom_navigation);
        this.disableShiftMode(view);
        menu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Intent i;
                switch (item.getItemId()) {
                    case R.id.allThingsMenuItem:
                        i = new Intent(context, FilterActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        break;
                    case R.id.insertThingMenuItem:
                        i = new Intent(context, NewThingActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        break;
                    case R.id.searchThingMenuItem:
                        i = new Intent(context, SearchThingActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        break;
                    case R.id.notificationMenuItem:
                        i = new Intent(context, NotificationActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        break;

                }
                return true;
            }
        });
    }

    public void increaseIconSize(View view, Context context){
        int newSize = 12;
        BottomNavigationView botomNav = (BottomNavigationView) view.findViewById(R.id.bottom_navigation);
        BottomNavigationMenuView bottomNavMenu = (BottomNavigationMenuView) botomNav.getChildAt(0);
        final DisplayMetrics displayMetrics =context.getResources().getDisplayMetrics();
        for (int i = 0; i < bottomNavMenu.getChildCount(); i++) {
            final View iconView = bottomNavMenu.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams params = iconView.getLayoutParams();
            if (displayMetrics.densityDpi <= 320) newSize = 32;
            params.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, displayMetrics);
            params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, displayMetrics);
            iconView.setLayoutParams(params);
        }
    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(View view) {
        BottomNavigationView menu = (BottomNavigationView) view.findViewById(R.id.bottom_navigation);
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) menu.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BottomNavigationMenu", "Shift mode problem", e);
        } catch (IllegalAccessException e) {
            Log.e("BottomNavigationMenu", "Shift mode problem", e);
        }
    }
}
