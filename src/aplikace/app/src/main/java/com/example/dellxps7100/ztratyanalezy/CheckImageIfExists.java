package com.example.dellxps7100.ztratyanalezy;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 01.05.2018.
 */

public class CheckImageIfExists {
    MoreInfoActivity activity;
    String filename;

    CheckImageIfExists(MoreInfoActivity activity, String filename){
        this.activity = activity;
        this.filename = filename;
    }

    public void checkIfExists(){
        ServerInfo server = new ServerInfo();
        AuthInterceptor auth = new AuthInterceptor(activity);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();

        CheckIfImageExistsInterface service =  new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(CheckIfImageExistsInterface.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.checkIfImageExists(this.filename);

        request.enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 404) activity.noAdditionImages();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                activity.noAdditionImages();
            }
        });

    }
}
