package com.example.dellxps7100.ztratyanalezy;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Patrik on 03.04.2018.
 */

public class DeleteThing {

    MoreInfoActivity activity;
    String id;
    String password;

    DeleteThing(MoreInfoActivity activity, String id, String password){
        this.activity = activity;
        this.id = id;
        this.password = password;
    }

    public void makeDelete(){
        ServerInfo server = new ServerInfo();
        AuthInterceptor auth = new AuthInterceptor(activity);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(auth)
                .build();

        DeleteThingInterface service = new Retrofit.Builder().baseUrl(server.getServerAddress())
                .client(client).build().create(DeleteThingInterface.class);

        final retrofit2.Call<okhttp3.ResponseBody> request = service.makeDelete(this.id, this.password);

        request.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200){
                    activity.succDelete();
                }
                if (response.code() == 400){
                    activity.failDelete();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                activity.failDelete();
            }
        });

    }


}
